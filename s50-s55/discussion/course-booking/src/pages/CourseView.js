import {useState,useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom';
import {Container,Row,Col,Card,Button} from 'react-bootstrap';
import Swal2 from 'sweetalert2';

export default function CourseView(){
	const [name,setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const navigate = useNavigate();

	const {courseId} = useParams();


	console.log(courseId)0;
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(result=>result.json())
		.then(data=>{
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	},[])

const enroll = (courseId) => {
  fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      id: courseId
    })
  })
    .then(response => response.json())
    .then(data => {
      if(data){
      	Swal2.fire({
      		title: 'Succssfully Enrolled',
      		icon: 'success',
      		text: "You have Succssfully enrolled for this course!" 		
      	})
 		navigate('/courses');
 }else{
 	Swal2.fire({
 		title: 'Something went wrong',
 		icon: 'error',
 		text: 'Please try again!'
 	})
 }

    });
};



	return(
			<Container>
				<Row>
					<Col>
						<Card>
						     <Card.Body>
						       <Card.Title>{name}</Card.Title>
						       <Card.Subtitle>Description:
						       </Card.Subtitle>
						       <Card.Text>{description}</Card.Text>
						       <Card.Text>Price:</Card.Text>
						       <Card.Text>{price}</Card.Text>

						       <Card.Subtitle>Class Schedule:
						       </Card.Subtitle>
						       <Card.Text>8am - 5pm</Card.Text>
						       <Button variant="primary" onClick={()=>enroll(courseId)} >Enroll</Button>
						     </Card.Body>
						   </Card>
					</Col>
				</Row>
			</Container>
		)
}