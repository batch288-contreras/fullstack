import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';

export default function Register() {
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();
  const [isDisabled, setIsDisabled] = useState(true);
  const [ firstName, setFirstName ] = useState('');
  const [ lastName, setLastName ] = useState('');
  const [mobileNo, setMobileNo ] = useState('');



  // useEffect(() => {
  //   if (user) {
  //     navigate('/not-found');
  //   }
  // }, [user, navigate]);

  useEffect(() => {
    if (email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && password1.length > 6) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [email, password1, password2,firstName,lastName,mobileNo]);




  function register(event) {
    event.preventDefault();

    const requestBody = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password1,
      isAdmin: false,
      mobileNo: mobileNo
    };


    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(requestBody)
    })
    .then(response => response.json())
    .then(data => {
      
      if(!data){
        Swal2.fire({
          title: 'Email already exist',
          icon: 'error',
          text: 'Please try a different email or try to log in!'
        });
// console.log(data)

      }else {
        Swal2.fire({
          title: 'Register Complete',
          icon: 'success',
          text: 'You have successfully been registered!'
        });
        navigate('/');

      }       
    });
  }
  return (

    <Container className="mt-5">
    <Row>
    <Col className="col-6 mx-auto">
    <h1 className="text-center">Register</h1>
    <Form onSubmit={event => register(event)}>
    <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label>Email address</Form.Label>
    <Form.Control
    type="email"
    value={email}
    onChange={event => setEmail(event.target.value)}
    placeholder="Enter email"
    />
    </Form.Group>

    <Form.Group className="mb-3" controlId="formBasicPassword1">
    <Form.Label>Password</Form.Label>
    <Form.Control
    type="password"
    value={password1}
    onChange={event => setPassword1(event.target.value)}
    placeholder="Password"
    />
    </Form.Group>

    <Form.Group className="mb-3" controlId="formBasicPassword2">
    <Form.Label>Confirm Password</Form.Label>
    <Form.Control
    type="password"
    value={password2}
    onChange={event => setPassword2(event.target.value)}
    placeholder="Retype your nominated password"
    />
    </Form.Group>



    <Form.Group className="mb-3" controlId="formFirstName">
    <Form.Label>First Name:</Form.Label>
    <Form.Control
    type="firstName"
    value={firstName}
    onChange={event => setFirstName(event.target.value)}
    placeholder="Enter Your First Name"
    />
    </Form.Group>


    <Form.Group className="mb-3" controlId="formLastName">
    <Form.Label>Last Name:</Form.Label>
    <Form.Control
    type="lastName"
    value={lastName}
    onChange={event => setLastName(event.target.value)}
    placeholder="Enter Your Last Name"
    />
    </Form.Group>


    <Form.Group className="mb-3" controlId="formMobileNo">
    <Form.Label>Mobile Number:</Form.Label>
    <Form.Control
    type="mobileNo"
    value={mobileNo}
    onChange={event => setMobileNo(event.target.value)}
    placeholder="Enter Your Mobile Number"
    />
    </Form.Group>







    <p>
    Have an account already? <Link to="/login">Login here</Link>
    </p>

    <Button variant="primary" type="submit" disabled={isDisabled}>
    Submit
    </Button>
    </Form>
    </Col>
    </Row>
    </Container>
    );
}
