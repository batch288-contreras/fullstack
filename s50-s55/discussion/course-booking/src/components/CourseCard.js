import {Container, Row, Col, Card, Button} from 'react-bootstrap';
//import the useState hook from react
import {useState, useEffect} from 'react';
import {useContext} from 'react';
import UserContext from '../UserContext.js'
import {Link} from 'react-router-dom';




export default function CourseCard(props) {
	//object destructuring


	const {user} = {UserContext}

	const {_id, name, description, price,enrollees} = props.courseProp;

	//Use the state hook for this component to be able to store its state
	//States are use to keep track of information related to individual components

		//Syntax : const [getter, setter] = useState(initiaGetterValue);

	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(30);
	const [isDisabled, setIsDisabled] = useState(false);


	

	// console.log(count);

	// setCount(2);
	// console.log(count);


	//this function will be invoke when the button enroll is cliked.
	function enroll(){

		if(seat !== 0){
			setCount(count + 1);
			setSeat(seat - 1);
		}else{
			alert('No more seats available!');
		}
	}

	

	//the function or the side effect in our useEffect hook will be invoked or run on the initial loading of our application and when there is/are change/changes on our dependencies.

	//syntax: 
		//useEffect(side effetctc, [dependencies]);

	useEffect(()=>{
		
		if(seat === 0){
			//it will the state or value of isDisabled to true
			setIsDisabled(true);
		}

	}, [seat]);



	return(
		<Container>
			<Row>
				<Col className="col-12 mt-3">
					<Card>
					    <Card.Body>
					    	<Card.Title className="mb-3">{name}</Card.Title>
					    	<Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>

					        <Card.Subtitle>Enrollees:</Card.Subtitle>
					        <Card.Text>{count}</Card.Text>

					        {
					        	user !==null
					        	?
					        	<Button as = {Link} to= {`/courses/${_id}`}>Enroll</Button>
					        	:
					        	<Button as={Link} to ='/login'>Login to Entroll</Button>

					        }


					        
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	);
}
